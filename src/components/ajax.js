// Make QueryString into object
const getQuerystringAsObject = function(location) {
  console.log("ajax.js | ","getQuerystringAsObject called");
  var keyNvalueArray = location.search.slice(1).split('&');
  var queryObject = {};

  // set default start and size
  queryObject['start'] = 1;
  queryObject['size'] = 5;

  for( var i = 0 ; i<2; i++){
    var keyNvalue = keyNvalueArray[i].split('=');
    var key = keyNvalue[0];
    var value = keyNvalue[1];

    if ((key=='start' || key=='size') && value){
        queryObject[key] = parseInt(value);
    }
  }
  return queryObject;
};

// Get URI in AJAX request for live report
var getXHRRequestUriForLive = function(location) {
  console.log("ajax.js | ", "getXHRRequestUriForLive called");
  var queryObject = getQuerystringAsObject(location);
  var uri ='http://localhost:3300/altiface/reports/ranking/live' + '?start=' + queryObject['start'] + '&size=' +queryObject['size'];

  console.log("ajax.js | ", uri);
  return uri;
};

// Make AJAX instance for live report
var sendXHRRequestForLive = function(location, xhrLive) {
  console.log("ajax.js | ", "sendXHRRequestForLive called");

  // Set AJAX request for live report
  xhrLive.open('GET', getXHRRequestUriForLive(location), true);

  // set headers for dbserver recognize it as ajax request
  xhrLive.setRequestHeader('Content-Type', 'application/json');
  xhrLive.setRequestHeader("X-Requested-With", "XMLHttpRequest");

  // Send AJAX request
  xhrLive.send();
  return xhrLive;
};

export default sendXHRRequestForLive;
