import React from 'react';
import axios from 'axios';
import DailyReport from './DailyReport';
import sendXHRRequestForLive from './ajax';

class App extends React.Component {
  constructor(props) {
    console.log("App | constructor is called");
    super(props);
    this.refreshRanking = this.refreshRanking.bind(this);
  }


  render(){
    console.log('App | rendered');
    if(this.props.store.getState().rankingData === {}){
    } else{
      return(
        <div>
          <DailyReport rankingData={this.props.store.getState().rankingData}/>
          <span><button type="button" onClick={this.refreshRanking}>refresh</button></span>
        </div>
      );
    }
  }

  refreshRanking() {
    var appStore = this.props.store;
    console.log("App | refreshRanking is called");
    var xhrLive = new XMLHttpRequest();
    xhrLive.onload= function() {
      if(xhrLive.status ===200){
        var responseJson = JSON.parse(xhrLive.responseText);
        console.log(responseJson);
        const rankingDataJson = responseJson;
        appStore.dispatch(updateRankingData(rankingDataJson.data));
      }
    };
    xhrLive = sendXHRRequestForLive(window.location, xhrLive);
  }
}

/*
 * action
 */
const RANKING_DATA = 'RANKING_DATA';

function updateRankingData(data) {
  return {
    type: RANKING_DATA,
    rankingData: data
  };
}

export default App;
