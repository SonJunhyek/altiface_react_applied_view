import React from 'react';

class Ranker extends React.Component {
  render(){
    let thumbStyle ={
      padding: '1em',
      margin: 'auto'
    };
    console.log("ranker total time test | ", this.props);

    return(
      <div className="ranker col-sm-6 col-md-4" style={thumbStyle}>
        <div className="thumbnail">
          <img src={this.props.imgUri} alt="..."/>
          <div className="caption">
              <h3 className="name">{this.props.name}</h3>
              <p className="contents">{this.props.totalTime}</p>
              <p><a href="#" className="btn btn-default" role="button">Profile</a></p>
          </div>
        </div>
      </div>
    );
  }
}

export default Ranker;
