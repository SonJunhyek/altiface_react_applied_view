import React from 'react';
import Ranker from './Ranker';

class Ranking extends React.Component {
  constructor(props){
    super(props);
  }

  render(){
    console.log('Ranking | ','Ranking is rendered');
    var rankingArr = (this.props.rankingData);
    if(rankingArr[0]){
      return (
        <div className="row ranking">
          {rankingArr.map(function(ranker, i) {
                  return (<Ranker key= {ranker.person_id}
                                  name= {ranker.person_id.split('_')[2]}
                                  totalTime= {secondToTime(ranker.total_time)}
                                  imgUri= {ranker.channels[0].image_url}
                                  />);
                })}
        </div>
      );
    } else { console.log("Ranking | ","empty"); return null;}
  }
}

// TODO : make this component
// Change second(number) to time
var secondToTime = function( second ) {

    var sec = second%60;
    var upperMin = Math.round(second/60);
    var min = upperMin%60;
    var upperHour = Math.round(upperMin/60);
    var hour = upperHour%24;
    var day = Math.round(upperHour/24);
    var time = {"day":day,"hour":hour, "minute":min, "second": sec};


    var timeText ='';
    if(time.day!=0){
      timeText = timeText+time.day+" day(s) ";
    }
    if(time.hour!=0){
      timeText =timeText+time.hour+" hour(s) ";
    }
    if(time.minute!=0){
      timeText =timeText+time.minute+" minute(s) ";
    }
    if(time.second!=0){
      timeText =timeText+time.second+" second(s) ";
    }
    return timeText;
};

export default Ranking;
