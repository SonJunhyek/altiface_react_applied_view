import React from 'react';
import Ranking from './Ranking';

class DailyReport extends React.Component {
  constructor(props){
    super(props);
  }

  render(){
    console.log('DailyReport | rendered');
    let bottomLine={
      borderBottomColor: '#555555',
      borderBottomWidth: 0.5
    };

    return (
      <div className="daily-report">
        <h>Daily Report</h>
        <Ranking rankingData = {this.props.rankingData}/>
      </div>
    );
  }
}

export default DailyReport;
