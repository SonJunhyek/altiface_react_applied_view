import React from 'react';
import ReactDOM from 'react-dom';
import { createStore } from 'redux';

const RANKING_DATA = 'RANKING_DATA';

function updateRankingData(data) {
  return {
    type: RANKING_DATA,
    rankingData: data
  };
}

const initialState = {
  rankingData: [
    {"person_id":"jojungsuk_19801226_조정석",
    "total_time":8520,
    "channels":[{
      "name":"SBS",
      "image_url":"http://10.70.0.203:8093/images/jojungsuk_19801226_조정석/20161123/SBS/capture.png",
      "image_md5":"2ad0476efe89494a2fa15a8dd7b5d509",
      "programs":[{"name":"program","time":8520}]}]},
    {"person_id":"hyunwoo_19850118_현우",
      "total_time":4270,
      "channels":[{
        "name":"KBS2",
        "image_url":"http://10.70.0.203:8093/images/hyunwoo_19850118_현우/20161123/KBS2/capture.png",
        "image_md5":"86596317d60460ef1e683e05a2dd3fc7",
        "programs":[{"name":"program","time":4270}]}]},
    {"person_id":"gonghyojin_19800404_공효진",
      "total_time":1980,
      "channels":[{
        "name":"SBS",
        "image_url":"http://10.70.0.203:8093/images/gonghyojin_19800404_공효진/20161123/SBS/capture.png",
        "image_md5":"898e910170a3aa119e9aac4ff07f0f2d",
        "programs":[{"name":"program","time":1980}]}]},
    {"person_id":"chainpyo_19671014_차인표",
      "total_time":1150,
      "channels":[{
        "name":"KBS2",
        "image_url":"http://10.70.0.203:8093/images/chainpyo_19671014_차인표/20161123/KBS2/capture.png",
        "image_md5":"746b066802d6d9b03ca73ef36215f847",
        "programs":[{"name":"program","time":1150}]}]},
    {"person_id":"leedonggun_19800726_이동건",
      "total_time":1040,
      "channels":[{
        "name":"KBS2",
        "image_url":"http://10.70.0.203:8093/images/leedonggun_19800726_이동건/20161123/KBS2/capture.png",
        "image_md5":"cd86c2aeaa9e8d3eea93b7b1e01c384b",
        "programs":[{"name":"program","time":1040}]}]
    }
  ]
};

const updateRankingReducer = function(state = initialState, action){
  switch(action.type) {
    case RANKING_DATA:
      return Object.assign({}, state, {
        rankingData: action.rankingData
      });
    default:
      return state;
  }
}

const store = createStore(updateRankingReducer);

export default store;
