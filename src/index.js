import React from 'react';
import ReactDOM from 'react-dom';
import store from './redux';
import App from './components/App';
// get ajax data and

const render = () => {
    const rootElement = document.getElementById('root');
    ReactDOM.render(<App store={store}/>, rootElement);
};

store.subscribe(render);

render();
