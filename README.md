# political_report

This is web service view and controller that provides ranking informations about the people who were on TV and how long that is in real time.

# explain structure
 app(dir) : containing routers and controllers
 public(dir) : containing static files and bundle.js which is compiled from all react components in src directory
 server(dir) : containing original view.js file which would be builded to view.js file in root directory.
 src(dir) : containing react components and index.js which is for gather the components programitically.
 view(dir) : containing ejs files which will be rendered as html template.

# download babel-cli for transfy
> npm install -g babel-cli


type this command to make root view.js from server directory view.js and compile index.js to bundle.js
> npm run build
type this command to run server
> npm run start
