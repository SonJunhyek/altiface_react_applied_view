// Invoke 'strict' JavaScript mode
'use strict';
var http = require('http');
var path = require('path');
var dbserverInfo = require('../../config/dbserver.info');

// require data to db server
exports.request = function (req, res) {

  // Against AJAX request
  if (req.headers["x-requested-with"] == "XMLHttpRequest") {
    console.log("live.dbserver.controller.js | ","ajax request is called");
    // Send JSON request to db server
    var options = {
      host: dbserverInfo.host,
      port: dbserverInfo.port,
      path: '/altiface/reports/ranking/live?start='+req.query.start+'&size='+req.query.size,
    };
    console.log("check this", "live.dbserver.controller.js | ",options);
    http.get(options, function (response) {
      response.on('data', function (chunk) {
        console.log("body: " + chunk);
        res.send(chunk);
      });
      response.on('end', function() {
      })
    }).on('error', function (e) {
      console.log("Got error: "+ e.message);
    });
  }

  // Against HTTP request
  else {
    console.log("live.dbserver.controller.js | ","http request is called");
    res.render('index',{"start":req.query.start,"size":req.query.size});
  }
};
