// Invoke 'strict' JavaScript mode
'use strict';

// Load the module dependencies

var WebpackDevServer = require('webpack-dev-server');
var webpack = require('webpack');
var express = require('./config/express');
var path = require('path');
var fs = require('file-system');

// Create a new Express application instance
var app = express();

var devPort = 3330;
var port = 3300;

if (process.env.NODE_ENV == 'development') {
    console.log('Server is running on development mode');

    var config = require('./webpack.dev.config');
    var compiler = webpack(config);
    var devServer = new WebpackDevServer(compiler, config.devServer);
    devServer.listen(devPort, function () {
        console.log('webpack-dev-server is listening on port', devPort);
    });
}

app.get('/hello', function(req, res) {
    res.send('Welcome!');
});


app.get('/altiface/reports/:report/:term', function (req, res) {
    var report = req.params.report;
    var term = req.params.term;
    switch (report) {
        case 'ranking':
            switch (term) {
                case 'live':
                    console.log("in live");
                    res.sendFile(path.join(__dirname, 'public/index.html'));
                    break;

                default:
                    break;
            }
            break;

        default:
            break;
    }
});


app.listen(port, function () {
    console.log('server is running on port ', port);
});
